import Vue from 'vue'
import VueRouter from 'vue-router'
import NProgress from 'nprogress'
import store from '@/store/index'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'event-list',
    component: () => import(/* webpackChunkName: "event-list" */ '../views/EventList.vue'),
    props: true
  },
  {
    path: '/event/:id',
    name: 'event-show',
    component: () => import(/* webpackChunkName: "event-show" */ '../views/EventShow.vue'),
    props: true,
   
    //executado depois do beforeEach global
    beforeEnter(routeTo, routeFrom, next){
      store.dispatch('event/fetchEvent', routeTo.params.id)
      .then((event) => {
        routeTo.params.event = event
        next() //continua uma vez que a promise da chamada da API foi resolvida
      })
      .catch((error) => {
        //error.response existir e se seu status for 404
        if(error.response && error.response.status == 404){
          console.log("404")
          //em caso de erro, redireciona para 404 com o nome do resource faltando (neste caso será um evento)
          next({name: '404', params: {resource: 'event'}})
        }else{
          console.log("network issue")
          next({name: 'network-issue'})
        }
      })
    }
  },
  {
    path: '/event/create',
    name: 'event-create',
    component: () => import(/* webpackChunkName: "event-create" */ '../views/EventCreate.vue')
  },
  {
    path: '/404',
    name: '404',
    props: true,
    component: () => import(/* webpackChunkName: "not-found" */ '../views/NotFound.vue')
  },
  {
    path: '/network-issue',
    name: 'network-issue',
    component: () => import(/* webpackChunkName: "network-issue" */ '../views/NetworkIssue.vue')
  },
  {
    path: '/example',
    name: 'example',
    component: () => import(/* webpackChunkName: "example" */ '../views/Example.vue')
  },
  {
    path: '*',
    redirect: {name: '404', params: {resource: 'page'}}
  }
]

const router = new VueRouter({
  routes,
  mode: "history"
})

//start progress bar when routing begins
router.beforeEach((routeTo, routeFrom, next) => {
  NProgress.start()
  next()
})

//termina a barra de progresso quando o roteamento estiver prestes a terminar
router.afterEach(() => {
  NProgress.done()
})

export default router
